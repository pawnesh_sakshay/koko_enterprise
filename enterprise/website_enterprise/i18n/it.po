# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_enterprise
# 
# Translators:
# Paolo Valier <paolo.valier@hotmail.it>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: Paolo Valier <paolo.valier@hotmail.it>, 2017\n"
"Language-Team: Italian (https://www.transifex.com/odoo/teams/41243/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: website_enterprise
#: model:ir.ui.view,arch_db:website_enterprise.external_snippets
msgid "Form Builder"
msgstr "Generazione Form"

#. module: website_enterprise
#: model:ir.ui.view,arch_db:website_enterprise.user_navbar
msgid "Website"
msgstr "Sito Web"
