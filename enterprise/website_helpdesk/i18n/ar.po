# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_helpdesk
# 
# Translators:
# Mohammed Ibrahim <m.ibrahim@mussder.com>, 2017
# Shaima Safar <shaima.safar@open-inside.com>, 2017
# Mustafa Rawi <mustafa@cubexco.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: Mustafa Rawi <mustafa@cubexco.com>, 2017\n"
"Language-Team: Arabic (https://www.transifex.com/odoo/teams/41243/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "Description"
msgstr "الوصف"

#. module: website_helpdesk
#: model:website.menu,name:website_helpdesk.website_menu_helpdesk
msgid "Help"
msgstr "المساعدة"

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "Helpdesk"
msgstr "المساعدة"

#. module: website_helpdesk
#: model:ir.model,name:website_helpdesk.model_helpdesk_team
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "Helpdesk Team"
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.header_footer_custom
msgid "Helpdesk Team Customer Satisfaction"
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.content_new_team
msgid "New Team"
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.not_published_any_team
msgid "No teams have been published."
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "Submit an Issue"
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "Team Settings"
msgstr ""

#. module: website_helpdesk
#: model:ir.ui.view,arch_db:website_helpdesk.team
msgid "View my Issues"
msgstr ""
