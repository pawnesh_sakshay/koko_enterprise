# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * mrp_workorder
# 
# Translators:
# Martin Trigaux <mat@odoo.com>, 2017
# Viktor Basso <viktor@voit.no>, 2017
# Jorunn D. Newth <jdn@eyenetworks.no>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-02 11:50+0000\n"
"PO-Revision-Date: 2017-10-02 11:50+0000\n"
"Last-Translator: Jorunn D. Newth <jdn@eyenetworks.no>, 2017\n"
"Language-Team: Norwegian Bokmål (https://www.transifex.com/odoo/teams/41243/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: mrp_workorder
#: model:ir.model,name:mrp_workorder.model_mrp_production
msgid "Manufacturing Order"
msgstr "Produksjonsordre"

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.mrp_production_menu_planning
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_tree_view_planning
msgid "Manufacturing Orders"
msgstr "Produksjonsordre"

#. module: mrp_workorder
#: model:ir.actions.server,name:mrp_workorder.production_order_unplan_server_action
msgid "Mrp: Unplan Production Orders"
msgstr ""

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.menu_mrp_production_plan
msgid "Orders to Plan"
msgstr "Ordrer å planlegge"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_view_form_inherit_planning
msgid "Plan"
msgstr "Planlegg"

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.menu_mrp_workorder_production
msgid "Planning by Production"
msgstr ""

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.menu_mrp_workorder_workcenter
msgid "Planning by Workcenter"
msgstr ""

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_tree_view_planning
msgid "Quantity"
msgstr "Antall"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_view_form_inherit_planning
msgid "Scheduled Date"
msgstr "Planlagt dato"

#. module: mrp_workorder
#: model:ir.model.fields,field_description:mrp_workorder.field_mrp_production_date_planned_finished_wo
msgid "Scheduled End Date"
msgstr "Planlagt sluttdato"

#. module: mrp_workorder
#: model:ir.model.fields,field_description:mrp_workorder.field_mrp_production_date_planned_start_wo
msgid "Scheduled Start Date"
msgstr "Planlagt startdato"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_tree_view_planning
msgid "Total Qty"
msgstr "Totalt antall"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_tree_view_planning
msgid "Unit of Measure"
msgstr "Enhet"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_view_form_inherit_planning
msgid "Unplan"
msgstr ""

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.menu_mrp_production_pending
msgid "Waiting for Material Availability"
msgstr ""

#. module: mrp_workorder
#: model:ir.ui.menu,name:mrp_workorder.mrp_workorder_menu_planning
msgid "Work Orders"
msgstr "Arbeidsordrer"

#. module: mrp_workorder
#: model:ir.ui.view,arch_db:mrp_workorder.mrp_production_view_form_inherit_planning
msgid "to"
msgstr "til"
