# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * web_enterprise
# 
# Translators:
# Эдуард Манятовский <manyatovskiy@gmail.com>, 2017
# SV <sv@grimmette.ru>, 2017
# Martin Trigaux <mat@odoo.com>, 2017
# Denis Trepalin <zekarious@gmail.com>, 2017
# Gennady Marchenko <gennadym@gmail.com>, 2017
# Masha Koc <mariya.kos.ua@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-02 11:50+0000\n"
"PO-Revision-Date: 2017-10-02 11:50+0000\n"
"Last-Translator: Masha Koc <mariya.kos.ua@gmail.com>, 2017\n"
"Language-Team: Russian (https://www.transifex.com/odoo/teams/41243/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:207
#, python-format
msgid "(Enterprise Edition)"
msgstr "(Enterprise Edition)"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:66
#, python-format
msgid "1 month"
msgstr "1 месяц"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base_mobile.xml:8
#, python-format
msgid "Action"
msgstr "Действие"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/js/views/relational_fields.js:51
#, python-format
msgid "Add an Item"
msgstr ""

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:240
#: code:addons/web_enterprise/static/src/xml/base.xml:243
#: code:addons/web_enterprise/static/src/xml/base.xml:249
#: code:addons/web_enterprise/static/src/xml/base.xml:252
#: code:addons/web_enterprise/static/src/xml/base.xml:258
#: code:addons/web_enterprise/static/src/xml/base.xml:261
#: code:addons/web_enterprise/static/src/xml/base.xml:267
#: code:addons/web_enterprise/static/src/xml/base.xml:270
#: code:addons/web_enterprise/static/src/xml/base.xml:276
#: code:addons/web_enterprise/static/src/xml/base.xml:279
#: code:addons/web_enterprise/static/src/xml/base.xml:285
#: code:addons/web_enterprise/static/src/xml/base.xml:288
#: code:addons/web_enterprise/static/src/xml/base.xml:294
#: code:addons/web_enterprise/static/src/xml/base.xml:297
#: code:addons/web_enterprise/static/src/xml/base.xml:303
#: code:addons/web_enterprise/static/src/xml/base.xml:306
#: code:addons/web_enterprise/static/src/xml/base.xml:312
#: code:addons/web_enterprise/static/src/xml/base.xml:315
#, python-format
msgid "Alt"
msgstr "Alt"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base_mobile.xml:38
#, python-format
msgid "COMPANIES"
msgstr ""

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:243
#: code:addons/web_enterprise/static/src/xml/base.xml:252
#: code:addons/web_enterprise/static/src/xml/base.xml:261
#: code:addons/web_enterprise/static/src/xml/base.xml:270
#: code:addons/web_enterprise/static/src/xml/base.xml:279
#: code:addons/web_enterprise/static/src/xml/base.xml:288
#: code:addons/web_enterprise/static/src/xml/base.xml:297
#: code:addons/web_enterprise/static/src/xml/base.xml:306
#: code:addons/web_enterprise/static/src/xml/base.xml:315
#, python-format
msgid "Control"
msgstr "Контроль"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:265
#, python-format
msgid "Create a new record"
msgstr "Создать новую запись"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:213
#, python-format
msgid "Database expiration:"
msgstr "Срок Базы Данных истекает:"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:231
#, python-format
msgid "Description"
msgstr "Описание"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:256
#, python-format
msgid "Discard a record modification"
msgstr "Отменить изменение записи"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:247
#, python-format
msgid "Edit a record"
msgstr "Редактировать запись"

#. module: web_enterprise
#: model:ir.model,name:web_enterprise.model_ir_http
msgid "HTTP routing"
msgstr "HTTP маршрутизация"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/js/widgets/user_menu.js:57
#, python-format
msgid "Keyboard Shortcuts"
msgstr "Горячие клавиши"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:97
#, python-format
msgid "Log in as an administrator to correct the issue."
msgstr "Войти как администратор, чтобы исправить эту проблему."

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:233
#, python-format
msgid "Mac"
msgstr "Mac"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:47
#, python-format
msgid "No result"
msgstr "Безрезультатно"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:206
#, python-format
msgid "Odoo"
msgstr "Odoo"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:110
#, python-format
msgid "Odoo Help"
msgstr "Odoo помощь"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:301
#, python-format
msgid "Open the next record"
msgstr "Откроыть следующую запись"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:292
#, python-format
msgid "Open the previous record"
msgstr "Открыть предыдущую запись"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:283
#, python-format
msgid "Open to kanban view"
msgstr "Открыть на канбан вид"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:274
#, python-format
msgid "Open to list view"
msgstr "Открыть в виде списка"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:118
#, python-format
msgid "Paste code here"
msgstr "Вставьте код здесь"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:89
#: code:addons/web_enterprise/static/src/xml/base.xml:94
#, python-format
msgid "Refresh subscription status"
msgstr "Обновить статус подписки"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:120
#, python-format
msgid "Register"
msgstr "Регистрация"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:85
#, python-format
msgid "Register your subscription"
msgstr "Зарегистрировать вашу подписку"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:88
#, python-format
msgid "Renew your subscription"
msgstr "Продлить вашу подписку"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:238
#, python-format
msgid "Save a record"
msgstr "Сохранить запись"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:7
#, python-format
msgid "Search..."
msgstr "Поиск..."

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:220
#, python-format
msgid "Shortcuts"
msgstr "Горячие клавиши"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:109
#, python-format
msgid ""
"Something went wrong while registering your database. You can try again or "
"contact"
msgstr ""
"Что-то пошло не так во время регистрации базы данных. Вы можете попробовать "
"еще раз или связаться"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:115
#, python-format
msgid "Subscription Code:"
msgstr "Код Подписки:"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:103
#, python-format
msgid ""
"Thank you, your registration was successful! Your database is valid until"
msgstr ""
"Спасибо, ваша регистрация прошла успешно! Ваша база данных действительна до"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:79
#, python-format
msgid "This database has expired."
msgstr "Срок этой базы данных истек."

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:75
#, python-format
msgid "This database will expire in"
msgstr "Срок этой базы данных истекает"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:72
#, python-format
msgid "This demo database will expire in"
msgstr "Срок этой демо базы данных истекает"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:310
#, python-format
msgid "Toggle home menu"
msgstr "Переключить главное меню"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:93
#, python-format
msgid "Upgrade your subscription"
msgstr "Обновите подписку"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:232
#, python-format
msgid "Windows/Linux"
msgstr "Windows/Linux"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:91
#, python-format
msgid "You have more users than your subscription allows."
msgstr "У вас есть больше пользователей, чем ваша подписка позволяет."

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:61
#, python-format
msgid ""
"You will be able to register your database once you have installed your "
"first app."
msgstr ""
"Вы сможете зарегистрировать свою базу данных, как только вы установили свое "
"первое приложение."

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:118
#, python-format
msgid "Your subscription code"
msgstr "Ваш код подписки"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:106
#, python-format
msgid "Your subscription was updated and is valid until"
msgstr "Ваша подписка обновлена и действительна до"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:85
#, python-format
msgid "buy a subscription"
msgstr "купить подписку"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:69
#, python-format
msgid "days"
msgstr "дней"

#. module: web_enterprise
#. openerp-web
#: code:addons/web_enterprise/static/src/xml/base.xml:85
#, python-format
msgid "or"
msgstr "или"
