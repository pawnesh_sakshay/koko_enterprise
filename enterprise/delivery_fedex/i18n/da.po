# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * delivery_fedex
# 
# Translators:
# Pernille Kristensen <pernillekristensen1994@gmail.com>, 2017
# Sanne Kristensen <sanne@vkdata.dk>, 2017
# Martin Trigaux, 2017
# lhmflexerp <lhm@flexerp.dk>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: lhmflexerp <lhm@flexerp.dk>, 2017\n"
"Language-Team: Danish (https://www.transifex.com/odoo/teams/41243/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid ", click on \"Get Production Key\" and follow all the steps."
msgstr ", Klik på \"Få produktionsnøgle\" og følg alle trin."

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid ""
"<br/>According to your needs, you will need to contact different "
"certifications :"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_account_number
msgid "Account Number"
msgstr "Kontonummer"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Advanced Services with Label Certification"
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Advanced Services without Label Certification"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_droppoff_type:0
msgid "BUSINESS_SERVICE_CENTER"
msgstr "BUSINESS_SERVICE_CENTER"

#. module: delivery_fedex
#: model:ir.model,name:delivery_fedex.model_delivery_carrier
msgid "Carrier"
msgstr "Fragtfirma"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Certification Process"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_droppoff_type:0
msgid "DROP_BOX"
msgstr "DROP_BOX"

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_default_packaging_id
msgid "Default Package Type"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_developer_key
msgid "Developer Key"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_file_type:0
msgid "EPL2"
msgstr ""

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:197
#, python-format
msgid ""
"Error:\n"
"%s"
msgstr ""
"Fejl:\n"
"%s"

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_label_file_type
msgid "FEDEX Label File Type"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "FEDEX_2_DAY"
msgstr "FEDEX_2_DAY"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "FEDEX_2_DAY_AM"
msgstr "FEDEX_2_DAY_AM"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "FEDEX_GROUND"
msgstr "FEDEX_GROUND"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "FIRST_OVERNIGHT"
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.res_config_settings_view_form_sale
#: model:ir.ui.view,arch_db:delivery_fedex.res_config_settings_view_form_stock
msgid "FedEx Delivery Methods"
msgstr "FedEx Leveringsmetoder"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "FedEx Web Services \"Move to Production\""
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Fedex Configuration"
msgstr "Fedex Konfiguration"

#. module: delivery_fedex
#: model:product.product,name:delivery_fedex.product_product_delivery_fedex_inter
#: model:product.template,name:delivery_fedex.product_product_delivery_fedex_inter_product_template
msgid "Fedex International"
msgstr "Fedex International"

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_label_stock_type
msgid "Fedex Label Stock Type"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_service_type
msgid "Fedex Service Type"
msgstr "Fedex Service Type"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Fedex Tutorial"
msgstr "Fedex Guide"

#. module: delivery_fedex
#: model:product.product,name:delivery_fedex.product_product_delivery_fedex_us
#: model:product.template,name:delivery_fedex.product_product_delivery_fedex_us_product_template
msgid "Fedex US"
msgstr "Fedex US"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Fedex Website"
msgstr "Fedex Hjemmeside"

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_weight_unit
msgid "Fedex Weight Unit"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_droppoff_type
msgid "Fedex drop-off type"
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Go to"
msgstr "Gå til"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "INTERNATIONAL_ECONOMY"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "INTERNATIONAL_PRIORITY"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_weight_unit:0
msgid "KG"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_weight_unit:0
msgid "LB"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_meter_number
msgid "Meter Number"
msgstr ""

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:379
#, python-format
msgid "No packages for this picking"
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Once your account is created, go to"
msgstr "Når din konto er oprettet, gå til"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_4X6"
msgstr "PAPER_4X6"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_4X8"
msgstr "PAPER_4X8"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_4X9"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_7X4.75"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_8.5X11_BOTTOM_HALF_LABEL"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_8.5X11_TOP_HALF_LABEL"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "PAPER_LETTER"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_file_type:0
msgid "PDF"
msgstr "PDF"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_file_type:0
msgid "PNG"
msgstr "PNG"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "PRIORITY_OVERNIGHT"
msgstr ""

#. module: delivery_fedex
#: model:ir.model,name:delivery_fedex.model_product_packaging
msgid "Packaging"
msgstr "Emballering"

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_developer_password
msgid "Password"
msgstr "Adgangskode"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_droppoff_type:0
msgid "REGULAR_PICKUP"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_droppoff_type:0
msgid "REQUEST_COURIER"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_service_type:0
msgid "STANDARD_OVERNIGHT"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_droppoff_type:0
msgid "STATION"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X6"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X6.75_LEADING_DOC_TAB"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X6.75_TRAILING_DOC_TAB"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X8"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X9_LEADING_DOC_TAB"
msgstr ""

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_stock_type:0
msgid "STOCK_4X9_TRAILING_DOC_TAB"
msgstr ""

#. module: delivery_fedex
#: model:ir.model.fields,field_description:delivery_fedex.field_delivery_carrier_fedex_saturday_delivery
msgid "Saturday Delivery"
msgstr "Lørdagslevering"

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:402
#, python-format
msgid "Shipment N° %s has been cancelled"
msgstr "Ordre nr: %s er blevet anulleret"

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:366
#, python-format
msgid "Shipment created into Fedex <br/> <b>Tracking Number : </b>%s"
msgstr "Fragt oprettet med Fedex <br/> <b>Tracking Nummer : </b>%s"

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:323
#, python-format
msgid ""
"Shipment created into Fedex<br/><b>Tracking Numbers:</b> "
"%s<br/><b>Packages:</b> %s"
msgstr ""
"Fragt oprettet hos Fedex<br/><b>Trackingnummer:</b> %s<br/><b>Pakker:</b> %s"

#. module: delivery_fedex
#: model:ir.model.fields,help:delivery_fedex.field_delivery_carrier_fedex_saturday_delivery
msgid ""
"Special service:Saturday Delivery, can be requested on following days.\n"
"                                                                                 Thursday:\n"
"1.FEDEX_2_DAY.\n"
"Friday:\n"
"1.PRIORITY_OVERNIGHT.\n"
"2.FIRST_OVERNIGHT.\n"
"                                                                                 3.INTERNATIONAL_PRIORITY.\n"
"(To Select Countries)"
msgstr ""

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "Standard Services"
msgstr "Standard Servicer"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "The last step is the"
msgstr "Det sidste trin er"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid ""
"These certifications usually require that you contact the FedEx support team"
" by email."
msgstr ""

#. module: delivery_fedex
#: code:addons/delivery_fedex/models/delivery_fedex.py:203
#, python-format
msgid ""
"Warning:\n"
"%s"
msgstr ""
"Advarsel:\n"
"%s"

#. module: delivery_fedex
#: selection:delivery.carrier,fedex_label_file_type:0
msgid "ZPLII"
msgstr "ZPLII"

#. module: delivery_fedex
#: model:ir.ui.view,arch_db:delivery_fedex.view_delivery_carrier_form_with_provider_fedex
msgid "to create a FedEx account of the following type:"
msgstr ""
