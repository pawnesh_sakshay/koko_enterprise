# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * timesheet_grid_sale
# 
# Translators:
# Sanne Kristensen <sanne@vkdata.dk>, 2017
# Ejner Sønniksen <ejner@vkdata.dk>, 2017
# lhmflexerp <lhm@flexerp.dk>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.saas~18+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-20 11:33+0000\n"
"PO-Revision-Date: 2017-09-20 11:33+0000\n"
"Last-Translator: lhmflexerp <lhm@flexerp.dk>, 2017\n"
"Language-Team: Danish (https://www.transifex.com/odoo/teams/41243/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: timesheet_grid_sale
#: selection:res.config.settings,invoiced_timesheet:0
msgid "All recorded timesheets"
msgstr "Alle registrerede timesedler"

#. module: timesheet_grid_sale
#: model:ir.model,name:timesheet_grid_sale.model_account_analytic_line
msgid "Analytic Line"
msgstr "Analyse linje"

#. module: timesheet_grid_sale
#: selection:res.config.settings,invoiced_timesheet:0
msgid "Approved timesheets only"
msgstr "Kun godkendte timesedler"

#. module: timesheet_grid_sale
#: model:ir.ui.view,arch_db:timesheet_grid_sale.res_config_settings_view_form
msgid "Invoicing Policy"
msgstr "Faktureringspolitik"

#. module: timesheet_grid_sale
#: model:ir.ui.view,arch_db:timesheet_grid_sale.res_config_settings_view_form
msgid "Record time spent and invoice it based on:"
msgstr "Registrér brugt tid og fakturér det baseret på:"

#. module: timesheet_grid_sale
#: model:ir.model,name:timesheet_grid_sale.model_sale_order_line
msgid "Sales Order Line"
msgstr "Salgsordrelinje"

#. module: timesheet_grid_sale
#: model:ir.model.fields,field_description:timesheet_grid_sale.field_res_config_settings_invoiced_timesheet
msgid "Timesheets Invoicing"
msgstr "Fakturering af timesedler"

#. module: timesheet_grid_sale
#: model:ir.model,name:timesheet_grid_sale.model_res_config_settings
msgid "res.config.settings"
msgstr "res.config.settings"

#. module: timesheet_grid_sale
#: model:ir.model,name:timesheet_grid_sale.model_timesheet_validation
msgid "timesheet.validation"
msgstr "timesheet.validation"
